import QtQuick 2.9
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import Morph.Web 0.1
import "UCSComponents"
import QtWebEngine 1.7
import Qt.labs.settings 1.0
import QtQuick.Window 2.2
import QtSystemInfo 5.5


MainView {
    id: mainview
    objectName: 'mainView'
    theme.name: "Ubuntu.Components.Themes.Ambiance"
    applicationName: 'optimaitalia.giampy'
    anchorToKeyboard: true
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    WebEngineView {
      id: webview
      anchors {
        fill: parent
      }
      property var currentWebview: webview

      profile:  WebEngineProfile {
        id: oxideContext
        httpUserAgent: "Mozilla/5.0 (Linux; Android 4.4; Nexus 5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.102 Mobile Safari/537.36 Ubuntu Touch Webapp"
        persistentStoragePath: dataLocation
        persistentCookiesPolicy: WebEngineProfile.ForcePersistentCookies
      }

      zoomFactor: 2.3
      url: "https://areaclienti.optimaitalia.com/login"
      userScripts: [
        WebEngineScript {
          injectionPoint: WebEngineScript.DocumentCreation
          worldId: WebEngineScript.MainWorld
          name: "QWebChannel"
          sourceUrl: "ubuntutheme.js"
        }
      ]
    }

    RadialBottomEdge {
      id: nav
      visible: true
      actions: [
        RadialAction {
          id: reload
          iconName: "reload"
          onTriggered: {
            webview.reload()
          }
          text: qsTr("Reload")
        },

        RadialAction {
          id: forward
          enabled: webview.canGoForward
          iconName: "go-next"
          onTriggered: {
            webview.goForward()
          }
          text: qsTr("Forward")
        },

        RadialAction {
          id: home
          iconName: "home"
          onTriggered: {
            webview.url = 'https://areaclienti.optimaitalia.com/home'
          }
          text: qsTr("Home")
        },

        RadialAction {
          id: back
          enabled: webview.canGoBack
          iconName: "go-previous"
          onTriggered: {
            webview.goBack()
          }
          text: qsTr("Back")
        }
      ]
    }

    Connections {
      target: Qt.inputMethod
      onVisibleChanged: nav.visible = !nav.visible
    }
}
